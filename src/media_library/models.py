from django.db import models
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.conf import settings

class Media(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    alt_text = models.CharField(max_length=150, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    file = models.FileField(upload_to='uploads/%Y/%m/')
    uploaded_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    upload_date = models.DateTimeField(auto_now_add=True)
    # caption = models.TextField(blank=True, null=True)
    # file_name = models.TextField()
    # file_type = models.CharField(max_length=150)
    # file_size = models.CharField(max_length=20)
    # dimension = models.CharField(max_length=150, blank=True, null=True)
    
    def __str__(self):
        return self.name