# Generated by Django 3.0.4 on 2020-04-07 03:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media_library', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='media',
            name='caption',
        ),
        migrations.RemoveField(
            model_name='media',
            name='file_name',
        ),
        migrations.RemoveField(
            model_name='media',
            name='file_size',
        ),
        migrations.RemoveField(
            model_name='media',
            name='file_type',
        ),
    ]
