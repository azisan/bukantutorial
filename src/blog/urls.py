from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('category/', views.category_list, name='category_list'),
    path('tagged/', views.tag_list, name='tag_list'),

    path('category/<category_slug>/<slug>/', views.post_detail, name='post_detail'),
    path('category/<slug>/', views.post_list_by_category, name='post_list_by_category'),
    path('tagged/<slug>/', views.post_list_by_tag, name='post_list_by_tag'),
]