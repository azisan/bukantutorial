from django.db import models
from django.conf import settings
from django.utils import timezone
from django.urls import reverse

from media_library.models import Media
from martor.models import MartorField

class Tag(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True,max_length=200)

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse("blog:post_list_by_tag", kwargs={"slug":self.slug})

    class Meta:
        ordering = ['name']

class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True,max_length=200)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("blog:post_list_by_category", kwargs={"slug":self.slug})
    
    class Meta:
        ordering = ['name']

class Post(models.Model):
    meta_description = models.TextField()
    meta_keyword = models.CharField(max_length=200)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True,max_length=200)
    content = MartorField(default="")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=None, null=True, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    published_date = models.DateTimeField(blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)
    featured_image = models.ForeignKey(Media, on_delete=models.CASCADE, default=None, null=True, blank=True)
    tag = models.ManyToManyField(Tag)
    
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:post_detail", kwargs={"slug": self.slug, "category_slug": self.category.slug})

    class Meta :
        ordering = ["-published_date"]