from .models import Post, Category, Tag
from django.db.models import Count
from django.utils import timezone

def recent_post_list(request):
    """
        This func use for dinamically show recently published post list on the sidebar
    """

    recent_posts = Post.objects.filter(published_date__lte=timezone.now())[:5]

    return {
        'recent_posts': recent_posts,
    }

def category_list_sidebar(request):
    """
        This func use for dinamically show all categories, and counte on the sidebar
        dont use categories as context variable name because it will conflict while render in category_list.html
    """

    categories = Category.objects.annotate(counter=Count('post')).order_by('name')

    return {
        'categories_sidebar': categories,
    }

def tag_list_sidebar(request):
    """
        This func use for dinamically show all tags on the sidebar even tags is not use in post yet
    """
    
    tags = Tag.objects.all().order_by('name')

    return {
        'tags_sidebar': tags,
    }