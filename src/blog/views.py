from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from .models import Post, Category, Tag

# tag func
def post_list_by_tag(request, slug):
    """
        Show all posts with specified tag in post_list.html
        if tag url wrong or tag not found then show 404
    """

    init_tag = get_object_or_404(Tag, slug=slug)
    get_posts = init_tag.post_set.filter(tag=init_tag, published_date__lte=timezone.now())
    paginator = Paginator(get_posts, 6) # Show n posts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)

    context = {
        'posts': posts,
    }
    return render(request, 'blog/post_list.html', context)

def tag_list(request):
    """
        Show all tags
    """
    tags = Tag.objects.all()
    context= {
        'tags': tags,
    }
    return render(request, 'blog/tag_list.html', context)

# category func
def post_list_by_category(request, slug):
    """
        Show all posts with specified category in post_list.html
        if category url wrong or category not found then show 404
    """
    # bisa juga pake ini doangan, cuman kalo categorynya gak ada dia nampilin kosongan bukan 404
    # posts = Post.objects.filter(category__slug=slug)
    
    init_category = get_object_or_404(Category, slug=slug)
    get_posts = init_category.post_set.filter(category=init_category, published_date__lte=timezone.now())
    paginator = Paginator(get_posts, 6) # Show n posts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)

    context = {
        'posts': posts,
    }
    return render(request, 'blog/post_list.html', context)

def category_list(request):
    """
        Show all categories
    """
    categories = Category.objects.all()
    context= {
        'categories': categories,
    }
    return render(request, 'blog/category_list.html', context)

# post func
def post_list(request):
    """
        Show all published posts in post_list.html
    """
    get_posts = Post.objects.filter(published_date__lte=timezone.now())

    # search
    search = request.GET.get("search")
    if search:
        get_posts = get_posts.filter(
            Q(title__icontains=search) |
            Q(content__icontains=search)
            )
    
    paginator = Paginator(get_posts, 6) # Show n posts per page.
    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)
    context= {
        'posts': posts,
    }
    
    return render(request, 'blog/post_list.html', context)

def post_detail(request, slug, category_slug):
    """
        Show detail published post in post_detail.html
        if post url wrong or post not found then show 404
    """
    post = get_object_or_404(Post, slug=slug)
    context = {
        'post': post,
    }
    return render(request, 'blog/post_detail.html', context)