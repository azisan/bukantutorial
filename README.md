# This is simple blog built with django

Installation : 

  - Clone this repo in directory
  - Create virtualenv with this command => `virtualenv envblog`
  - Activate envblog => `source envblog/bin/activate`
  - Change dir => `cd bukantutorial/src`
  - Install dependencies => `pip install -r requirements.txt`
  - Runserver => `python manage.py runserver`

Super user :

  - Username : admin
  - Password : admin
